import React from 'react';
import Days from '../lib/Days';
import Month from '../lib/Month';
import './clockApp.css';

// Default properties values to show static time as the example picture
const Clock = (
    {
    day, 
    hour, 
    minutes, 
    month, 
    dayOfMonth, 
    year
    }
) => {
    if(!hour || !minutes){
        return (
            <div className="app">
                <div className="content">
                    <section className="isLoading">
                        Loading...
                    </section>
                </div>
            </div>
        )
    }

    return (
        <div className="app">
            <div className="content">
                <span className="dayOfTheWeek">
                    {Days.getDay(day)}
                </span>
                <span className="dayOPeriod">
                    {`${hour < 10 ? 'Morning' : 'Afternoon'}`}
                </span>
                <section className="currentTime">
                    <span>{`${hour < 10 ? '0'+hour : hour} : ${minutes < 10 ? '0'+minutes: minutes}`}</span>
                </section>
                <span className="date">
                    {dayOfMonth} {Month.getMonth(month)} {year}
                </span>
            </div>
        </div>
    )
}

export default Clock