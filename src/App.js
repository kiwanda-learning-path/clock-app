import React, { useEffect, useState } from 'react';
import './App.css';
import ClockApp from './components/ClockApp';

function App(){
    // Needed properties
    const [day, setDay] = useState()
    const [month, setMonth] = useState()
    const [year, setYear] = useState()
    const [hour, setHour] = useState()
    const [dayOfMonth, setDayOfMonth] = useState()
    const [minutes, setMinutes] = useState()

    // Once component is mounted
    useEffect(()=>{
        setInterval(()=>{
            const currentDate = new Date()
            setDay(currentDate.getDay())
            setMonth(currentDate.getMonth())
            setYear(currentDate.getFullYear())
            setHour(currentDate.getHours())
            setDayOfMonth(currentDate.getDate())
            setMinutes(currentDate.getMinutes())
        }, 1000)
    }, [])

    return <div className="main">
        <section className="clock">
            {/* properties are passed to our ClockApp component */}
            <ClockApp 
                day={day}
                month={month}
                year={year}
                hour={hour}
                dayOfMonth={dayOfMonth}
                minutes={minutes}
            />
        </section>
    </div>
}

export default App