# Learning `React JS` | [kiwanda](https://kiwanda.org/), Clock Application

This is a `Clock App` application, assigned by <strong>kiwanda</strong>'s instructor as second test on our ways to become <strong>React JS Experts</strong>. Hosted on firebase at [clock-app-leoka](https://kiwanda-clock-app-leoka.web.app/).

## Technologies

- React v17
<!-- - Bulma v0.9.1 -->
- Firebase (Hosting)

## Credit

Happy coding! 🔥 